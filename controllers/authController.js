const jwt = require('jsonwebtoken');
const User = require('./../model/userModel');

//({Payload}, secret) The Payload we want in our jwt
//the header is created automatically
const signToken = (id) => {
  jwt.sign({ id }, process.env.JWT_SECRET, {
    expiresIn: process.env.JWT_EXPIRES_IN,
  });
};

//Log the user in with a toekn when they Sign Up
exports.signup = async (req, res, next) => {
  try {
    const { name, email, password, passwordConfirm } = req.body; //destructuring pulling out the properties of req.body
    const newUser = await User.create({ name, email, password, passwordConfirm });

    //rmr in mongoDB the the id is _id
    const token = signToken(newUser._id);

    res.status(201).json({ token, newUser }); //Place the const token in the .json object to send it to the client
  } catch (err) {
    res.status(501).json(err);
  }
};

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body; //user sends in login credentials for us to check

    //1) check if email and password exist

    if (!email || !password) {
      throw new Error('Please provide email and password!'); //400
    }

    //2) check if user exists && password correct

    const user = await User.findOne({ email }).select('+password'); //to get a feild from DB that is not selectd us '+" and then name of the field"
    console.log(user);

    //Function call 'correctPassword' to compar password with hashedPassword
    if (!user || !(await user.correctPassword(password, user.password))) {
      throw new Error('Incorrect email or password!'); //401
    }

    //3) if everything ok, send toekn to client
    const token = signToken(user._id);
    res.status(200).json({
      status: 'success',
      token,
    });
  } catch (err) {
    res.status(501).json(err);
  }
};
